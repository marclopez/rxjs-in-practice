import {Component, OnInit} from '@angular/core';
import {Course} from "../model/course";
import {Observable} from 'rxjs';
import {Store} from "../common/store.service";


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  beginnersCourses$: Observable<Course[]>;
  advancedCourses$: Observable<Course[]>;

    constructor(private store: Store) {}

    ngOnInit() {
      /** beginnerCourses i advancedCourses son carregats per async pipe al html, no requereixen subscripcio manual
       * deriven del observable creat en el store service
       */
      this.beginnersCourses$ = this.store.selectBeginnerCourses();
      this.advancedCourses$ = this.store.selectAdvancedCourses();
    }

}
