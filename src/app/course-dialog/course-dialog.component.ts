import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import {Course} from "../model/course";
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import * as moment from 'moment';
import {exhaustMap, filter, mergeMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import {fromEvent} from "rxjs";
import {Store} from "../common/store.service";

@Component({
    selector: 'course-dialog',
    templateUrl: './course-dialog.component.html',
    styleUrls: ['./course-dialog.component.css']
})
export class CourseDialogComponent implements OnInit, AfterViewInit {

    form: FormGroup;
    course:Course;

    @ViewChild('saveButton', { static: true , read: ElementRef}) saveButton: ElementRef;

    @ViewChild('searchInput', { static: true }) searchInput : ElementRef;

    constructor(
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<CourseDialogComponent>,
        private store: Store,
        @Inject(MAT_DIALOG_DATA) course:Course ) {

        this.course = course;

        this.form = fb.group({
            description: [course.description, Validators.required],
            category: [course.category, Validators.required],
            releasedAt: [moment(), Validators.required],
            longDescription: [course.longDescription,Validators.required]
        });

    }

    ngOnInit() {
      this.form.valueChanges
        .pipe(
          // filtra si el formulari es valid
          filter(() => this.form.valid),
          // guarda el resultat de cada modificacio, asincronicament
          // en aquest cas no seria ideal
          mergeMap(changes => this.saveCourse(changes))
        )
        .subscribe();


    }

    private saveCourse(changes) {
      return fromPromise(fetch(`/api/courses/${this.course.id}`,{
        method: 'PUT',
        body: JSON.stringify(changes),
        headers: {
          'content-type': 'application/json'
        }
      }));
    }



    ngAfterViewInit() {
      fromEvent(this.saveButton.nativeElement, 'click')
        .pipe(
          // exhaustMap ignora els streams d'altres observables iguals durant la seva execucio
          exhaustMap(() => this.saveCourse(this.form.value))
        )
        .subscribe();

    }

    save() {
      this.store.saveCourse(this.course.id, this.form.value)
        .subscribe(
          () => this.close(),
          err => console.log("error saving course", err)
        );
    }


    close() {
        this.dialogRef.close();
    }

}
