import {Component, OnInit } from '@angular/core';
import {ReplaySubject} from "rxjs";

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
   // emet els valors sense necessitat de completar-se, tot i que es subscrigui mes tard
    const subject = new ReplaySubject();
    const series$ = subject.asObservable();
    series$.subscribe(val => console.log('early: '+val));

    subject.next(1);
    subject.next(2);
    subject.next(3);
    // subject.complete();

    setTimeout(() => {
      series$.subscribe(val => console.log('late: '+val));
    }, 1000)
  }


}


